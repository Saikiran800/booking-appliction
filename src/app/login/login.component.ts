import { Component } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email:any;
  password:any;

  constructor(private service: CustomerService, private router: Router ){

  }

  login(loginForm:any){
    console.log('login form :'+loginForm);
    this.email = loginForm.email;
    this.password = loginForm.password;
    console.log(this.email+""+this.password);

    this.service.getUser(this.email,this.password).subscribe((userData:any)=>{
      console.log(userData);
       //this.router.navigate(['/home']);
    });


  }

}
