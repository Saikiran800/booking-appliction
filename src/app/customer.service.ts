import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  
  constructor(private http: HttpClient) { }


  register(user: any) {
    return this.http.post('http://localhost:8085/registerCustomer/', user) ;
  }

  getUser(email:any,password:any):any{
    return this.http.get('http://localhost:8085/login/'+email+'/'+password) ;
  }

}
